﻿using Iska;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace TryIsca
{
    /// <summary>
    /// Hypotetical test application to test Isca Session Library.
    /// 
    /// Created by: Celestine Ezeokoye. 6th Sept, 2013.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            //Create model objects that would be persisted in session
            var user1 = new User("Celestine", "Ezeokoye", 1);
            var user2 = new User("Ifetayo", "Agunbiade", 2);
            var admin1 = new Admin("Admin", "Celestine", "Super-Admin", 1);
            var admin2 = new Admin("Admin", "Ifetayo", "Admin", 2);

            //Initialize the session
            ServerSession.Init(new BinaryStorage(), 300000, 6000);

            /*
             * Add then to the session and get a session key.
             * Usually, this key is then passed back and forth, and used to ensure
             * that the user is in session & has permission to access the resources/serices that they request.
             */
            var key1 = ServerSession.Add(user1);
            var key2 = ServerSession.Add(user2);
            var key3 = ServerSession.Add(admin1);
            var key4 = ServerSession.Add(admin2);

            Thread.Sleep(8000);

            //Retrieve session
            var sessData1 = ServerSession.Get(key1);
            var sessData2 = ServerSession.Get(key4);

            //Filter access to resources based on type.
            if (sessData1.GetType() == typeof(User))
            {
                //Grant access to User specific resources.
                Console.WriteLine(sessData1.GetType());
            }

            if (sessData2.GetType() == typeof(Admin))
            {
                //Grant access to Admin resources
                Console.WriteLine(sessData2.GetType());
            }

            Console.Read();
        }
    }


    /*
     * Entities used by our hypothetical test application
     */
    [Serializable]
    class User
    {
        public string Firstname, Lastname;
        public int UserId;

        public User(string firstname, string lastname, int id)
        {
            Firstname = firstname;
            Lastname = lastname;
            UserId = id;
        }
    }

    [Serializable]
    class Admin
    {
        public string Firstname, Lastname, Role;
        public int AdminId;

        public Admin(string firstname, string lastname, string role, int id)
        {
            Firstname = firstname;
            Lastname = lastname;
            Role = role;
            AdminId = id;
        }
    }

    /*
     * Class encapsulating the procedures for persisting/backing-upp the session data.
     * 
     * Session data can be backed-up using any desired means: File system, XML serialization,
     * Azure blob storage, database, etc. In this example, I used local binary storage.
     */
    class BinaryStorage : IStorage
    {
        private string store;

        public BinaryStorage()
        {
            store = "SessionStore.bin";
        }

        public void Write(HashSet<SessionData> data)
        {
            using (FileStream fs = new FileStream(store, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None))
            {
                var bf = new BinaryFormatter();
                bf.Serialize(fs, data);

                fs.Flush();
                fs.Position = 0;
            }
        }

        public HashSet<SessionData> Read()
        {
            using (FileStream fs = new FileStream(store, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
            {
                var bf = new BinaryFormatter();

                if (fs.Length > 0)
                    return (HashSet<SessionData>)bf.Deserialize(fs);
                else
                    return null;
            }
        }
    }
}
